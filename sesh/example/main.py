
"""
sesh is a session management library for FastAPI

Copyright (C) 2024  Brian Farrell

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Contact: brian.farrell@me.com
"""


from fastapi import FastAPI

from sesh.example.api import router

from .config import config


def get_app() -> FastAPI:
    app = FastAPI(title=config.project_name, version=config.version)

    @app.get("/api/", name="api:get-api", tags=["api"])
    def get_api() -> dict:
        return app.openapi()

    app.include_router(router, prefix="/api")

    return app
