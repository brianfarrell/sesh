
"""
sesh is a session management library for FastAPI

Copyright (C) 2024  Brian Farrell

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Contact: brian.farrell@me.com
"""


from datetime import date
from typing import List, Optional
from typing_extensions import Annotated

from email_validator import validate_email
from pydantic import BaseModel, Field, field_validator, HttpUrl, StringConstraints


class Address(BaseModel):
    addr_label: Annotated[str, StringConstraints(to_lower=True), Field(min_length=3, max_length=20)]  # type: ignore
    addr_primary: bool
    addr1: Annotated[str, Field(min_length=3, max_length=100)]
    addr2: Annotated[str, Field(max_length=100)]
    city: Annotated[str, Field(min_length=2, max_length=50)]
    state: Annotated[str, Field(min_length=2, max_length=2)]
    zip_code: Annotated[str, Field(min_length=5, max_length=5)]


class EmailAddress(BaseModel):
    email_label: Annotated[str, StringConstraints(to_lower=True), Field(min_length=3, max_length=12)]  # type: ignore
    email_address: str
    verified: bool
    email_primary: bool

    @classmethod
    @field_validator('email_address')
    def validate_email_address(cls, value: str) -> str:
        email_info = validate_email(value)
        return email_info.normalized


class PhoneNumber(BaseModel):
    phone_label: Annotated[str, Field(max_length=12)]
    phone_number: Annotated[str, Field(max_length=15)]
    phone_primary: bool


class PlatformDict(BaseModel):
    platform: Annotated[str, Field(max_length=30)]
    url: HttpUrl
    handle: Annotated[str, Field(max_length=30)]


class ProfileData(BaseModel):
    nickname: Optional[str] = None
    dob: Optional[Annotated[date, Field(description="Date Of Birth in format 'YYYY-MM-DD'", title="Birthday")]] = None
    occupation: Optional[Annotated[str, Field(max_length=50)]] = None
    company: Optional[Annotated[str, Field(max_length=100)]] = None
    title: Optional[Annotated[str, Field(max_length=30)]] = None
    image_file: Optional[str] = None
    bio: Optional[str] = None
    addresses: Optional[List[Address]] = None
    email_addresses: Optional[List[EmailAddress]] = None
    phone_numbers: Optional[List[PhoneNumber]] = None
    platforms: Optional[List[PlatformDict]] = None
    favorite_color: Optional[str] = None
    favorite_movie_genres: Optional[list[str]] = None
