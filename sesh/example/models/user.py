
"""
sesh is a session management library for FastAPI

Copyright (C) 2024  Brian Farrell

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Contact: brian.farrell@me.com
"""


from datetime import datetime
from typing import List, Optional, Self
from typing_extensions import Annotated

from aenum import UniqueEnum
from email_validator import validate_email
from pydantic import Field, field_validator, model_validator

from sesh.models.key import KeyBase

from .profile import ProfileData


class AppRole(UniqueEnum):
    ADMIN = 90     # Full privileges
    POWER = 80     # Manage users, issue refunds, run reports, manage inventory
    STAFF = 60     # Manage time, modify/process orders
    VENDOR = 40    # Supply compliance/quality assurance data, Check inventory
    CUSTOMER = 20  # Browse/Search inventory and place orders
    GUEST = 0      # No privileges


class UserGroup(UniqueEnum):
    ACCT = 'acct'
    CSTE = 'cste'
    HR = 'hr'
    IT = 'itech'
    MKTG = 'mktg'
    STAFF = 'staff'
    WHEEL = 'wheel'


class User(KeyBase):
    user_id: Optional[int] = None
    app_role: AppRole = AppRole.GUEST
    groups: List[UserGroup] = []
    name_first: Optional[str] = None
    name_last: Optional[str] = None
    email_label: Optional[str] = 'primary'
    email_address: Optional[str] = None
    created_at: Annotated[Optional[str], Field(validate_default=True)] = None
    updated_at: Annotated[Optional[str], Field(validate_default=True)] = None
    profile: Optional[ProfileData] = None

    @classmethod
    @field_validator('email_address')  # type: ignore
    def validate_email_address(cls, value: str) -> str:
        email_info = validate_email(value)
        return email_info.normalized

    @model_validator(mode='after')
    def default_datetime(self) -> Self:
        if not self.created_at:
            self.created_at = datetime.now().strftime('%Y-%m-%dT%H:%M:%S')
        if not self.updated_at:
            self.updated_at = datetime.now().strftime('%Y-%m-%dT%H:%M:%S')
        return self
