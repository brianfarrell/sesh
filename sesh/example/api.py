
"""
sesh is a session management library for FastAPI

Copyright (C) 2024  Brian Farrell

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Contact: brian.farrell@me.com
"""


from fastapi import APIRouter, Depends, HTTPException, Request, Response, status
from fastapi.responses import RedirectResponse
from pydantic import ValidationError

from sesh.dependencies.authentication import get_state, login_check, user_auth
from sesh.exceptions import SessionError
from sesh.models.cookie import CookiePayload

from .config import config
from .cookies import auth_session, user_session
from .mock_db import mock_db as persistent_db
from .models.auth import AuthData
from .models.content import ContentData
from .models.session import SessionData
from .models.user import User


router = APIRouter()


@router.post(
    "/login/",
    name="login",
    response_model=SessionData,
    response_model_exclude={"app_role", "groups", "key_id", "key_ttl"},
    response_model_exclude_none=True,
    status_code=status.HTTP_201_CREATED
)
async def login(
        body: dict,
        request: Request,
        response: Response,
        auth_data: dict = Depends(login_check(auth_session))
):
    authed_user = None
    if not auth_data:
        # In this example app, we assume that if they are in the database, they
        # are successfully authorized at login.  At a minimum, an actual login
        # function should include a check against a password hash.
        authed_user = persistent_db.get(body['email_address'])

        if authed_user:
            # If the user is found in the persistent_db, they are assumed to be authorized
            # Write the user's 'auth' data to the Redis cache.
            try:
                auth_data = AuthData(
                    key_id=config.new_key_id(),
                    key_ttl=config.auth_session_ttl,
                    **authed_user
                )
                await auth_session.backend.create_entry(auth_data)
            except ValidationError as e:
                raise HTTPException(status_code=422, detail=e.json(include_input=False).replace('"', ""))
            except SessionError:
                raise HTTPException(
                    status_code=503,
                    detail="There is a problem with the session validation service."
                )
        else:
            # User not found in database, so they must be a new user.
            # Ask them to sign-up.
            return RedirectResponse(url=f"http://{config.cookie_domain}/api/sign-up/")  # noqa
    else:
        # User's'auth' cookie from last visit is still valid.
        # 'Auth' data returned from cache.
        pass

    # Pack the cookie's payload and attach it to the response.
    payload = auth_session.pack_payload(
        CookiePayload(
            key_id=auth_data.key_id,
            key_ttl=auth_data.key_ttl,
            data_model=auth_session.cookie_type.model
        )
    )
    auth_session.attach_to_response(payload, response)

    # Get valid session data from the Redis cache, if it exists.
    session_data: SessionData = await user_session(request, response, login_route=True)

    if authed_user and not session_data:
        # Create new session data
        try:
            session_data: SessionData = SessionData(
                key_id=config.new_key_id(),
                key_ttl=config.session_ttl,
                **authed_user
            )
            await user_session.backend.create_entry(session_data)
        except ValidationError as e:
            raise HTTPException(status_code=422, detail=e.json(include_input=False).replace('"', ""))
        except SessionError:
            raise HTTPException(
                status_code=503,
                detail="There is a problem with the session validation service."
            )

    # Pack the cookie's payload and attach it to the response.
    payload = user_session.pack_payload(
        CookiePayload(
            key_id=session_data.key_id,
            key_ttl=session_data.key_ttl,
            data_model=user_session.cookie_type.model
        )
    )
    user_session.attach_to_response(payload, response)

    return session_data


@router.get(
    "/sign-up/",
    name="sign-up",
    status_code=status.HTTP_200_OK
)
async def sign_up(
):
    return {"message": "You've arrived at the sign-up page."}


@router.post(
    "/sign-up/",
    name="sign-up",
    status_code=status.HTTP_201_CREATED
)
async def sign_up(
):
    return {"message": "You've posted at the sign-up page."}


@router.get(
    "/profile/",
    response_model=SessionData,
    response_model_exclude={"app_role", "groups", "key_id", "key_ttl"},
    response_model_exclude_none=True,
    name="profile",
    status_code=status.HTTP_200_OK
)
async def get_profile(session_data: SessionData = Depends(get_state(user_session))):
    """
    User must be logged-in to see their profile
    """
    return session_data


@router.post(
    "/profile/update/",
    name="profile:update",
    response_model=SessionData,
    response_model_exclude={"app_role", "groups", "key_id", "key_ttl"},
    response_model_exclude_none=True,
    status_code=status.HTTP_201_CREATED
)
async def update_profile(user: User, session_data: SessionData = Depends(get_state(user_session))):
    updated_user = user
    updated_user.key_id = session_data.key_id
    updated_user.key_ttl = session_data.key_ttl

    try:
        await user_session.backend.update(updated_user)
    except SessionError:
        raise HTTPException(
            status_code=503,
            detail="There is a problem with the session validation service."
        )

    return updated_user


@router.post(
    "/content/new/",
    name="content:new",
    response_model=ContentData,
    response_model_exclude_none=True,
    status_code=status.HTTP_201_CREATED,
    dependencies=[Depends(user_auth(auth_session))]
)
async def post_new_content(content: ContentData):
    return content


@router.delete(
    "/logout/",
    name="logout",
    status_code=status.HTTP_204_NO_CONTENT
)
async def del_session(
    response: Response,
    session_data: SessionData = Depends(get_state(user_session))
):
    await user_session.backend.delete(session_data.key_id)

    # We want to delete the 'auth' cookie, so we're asking the `auth_session`
    # to make the cookie that we will then delete.
    auth_cookie = auth_session.make_cookie()
    user_session.delete_from_response(response, auth_cookie)

    return {"msg": "deleted session"}
