
"""
sesh is a session management library for FastAPI

Copyright (C) 2024  Brian Farrell

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Contact: brian.farrell@me.com
"""


from typing import Callable
from uuid import uuid4

from pydantic import RedisDsn
from pydantic_settings import BaseSettings, SettingsConfigDict


def new_key_id() -> str:
    """Generate a UUID4 value, but immediately cast it as a string.

    Returns: a string of a UUID4 value

    """
    return str(uuid4())


class Config(BaseSettings):
    model_config = SettingsConfigDict(env_prefix='example_')

    auth_cookie_salt: str
    auth_session_ttl: int

    backend: str
    cookie_domain: str

    deployment_stage: str

    session_cookie_http_only: bool
    session_cookie_salt: str
    session_cookie_same_site: str
    session_ttl: int

    project_name: str
    redis_dsn: RedisDsn
    secret_key: str

    version: str = "1.0"
    sid_generator: Callable = new_key_id

    def new_key_id(self) -> str:
        return self.sid_generator()


config = Config()  # type: ignore
