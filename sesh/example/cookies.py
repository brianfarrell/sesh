
"""
sesh is a session management library for FastAPI

Copyright (C) 2024  Brian Farrell

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Contact: brian.farrell@me.com
"""


from aenum import extend_enum

from sesh.backend.redis import RedisStore
from sesh.backend.base import SessionStore
from sesh.session.cookie import CookieSession
from sesh.models.cookie import CookieType

from .config import config
from .models.auth import AuthData
from .models.session import SessionData


for name, value in (
    ('AUTH', (AuthData, 'AuthData')),
    ('SESSION', (SessionData, 'SessionData')),
):
    extend_enum(CookieType, name, value)

redis_backend: SessionStore = RedisStore(config.redis_dsn)

auth_session = CookieSession(
    backend=redis_backend,
    cookie_type=CookieType.AUTH,
    created_by=1,
    domain=config.cookie_domain,
    max_age=config.auth_session_ttl,
    salt=config.auth_cookie_salt,
    secret_key=config.secret_key,
)

user_session = CookieSession(
    backend=redis_backend,
    cookie_type=CookieType.SESSION,
    created_by=1,
    domain=config.cookie_domain,
    max_age=config.session_ttl,
    salt=config.session_cookie_salt,
    secret_key=config.secret_key,
)
