
"""
sesh is a session management library for FastAPI

Copyright (C) 2024  Brian Farrell

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Contact: brian.farrell@me.com
"""


mock_db = {
    'dutchmorley@hotmail.com': {
        'user_id': 1,
        'groups': ['cste', 'wheel'],
        'app_role': 90,
        'name_first': 'David',
        'name_last': 'Morley',
        'email_label': 'home',
        'email_address': 'dutchmorley@hotmail.com',
        'created_at': '',
        'profile': None
    },
    'shron@luxebuds.com': {
        'user_id': 2,
        'groups': ['cste', 'mktg'],
        'app_role': 80,
        'name_first': 'Shron',
        'name_last': 'Guest',
        'email_label': 'work',
        'email_address': 'shron@luxebuds.com',
        'created_at': '',
        'profile': None
    },
    'anthony@luxebuds.com': {
        'user_id': 3,
        'groups': ['mktg'],
        'app_role': 60,
        'name_first': 'Anthony',
        'name_last': 'Bourdain',
        'email_label': 'primary',
        'email_address': 'anthony@luxebuds.com',
        'created_at': '',
        'profile': None
    },
    'seymour@luxebuds.com': {
        'user_id': 4,
        'groups': ['acct'],
        'app_role': 60,
        'name_first': 'Philip',
        'name_last': 'Hoffman',
        'email_label': 'primary',
        'email_address': 'seymour@luxebuds.com',
        'created_at': '',
        'profile': None
    },
    'matthew@luxebuds.com': {
        'user_id': 5,
        'groups': ['staff'],
        'app_role': 40,
        'name_first': 'Matthew',
        'name_last': 'Perry',
        'email_label': 'primary',
        'email_address': 'matthew@luxebuds.com',
        'created_at': '',
        'profile': None
    }
}
