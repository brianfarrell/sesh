
# Example App

An example app is provided in the repo that shows how to use sesh to manage
sessions for an api. The example app is not installed via pip. The best way
to take a look at the example app is to [clone this repository](https://gitlab.com/brianfarrell/sesh).
Whether you check it out or not, continue reading here as we walk through how the
example app is put together.

After you clone the repo, you can just go into the project directory and run
`docker compose -f docker-compose-example.yaml up -d` and you should have a
minimal version of the Example api up and running, with session management from
Sesh. Unless you've modified the setup, you should be able to go to
[http://127.0.0.1:8081/docs](http://127.0.0.1:8081/docs) to view the
OpenAPI documentation for the example app.

The `docker-compose-example.yaml` file is set up to read-in environment
variables or set reasonable default values for environment variables that are
not set.  If you want to take a good look at Sesh, it is highly recommended that
you create a `.env` file in the project root and set at least two environment
variables (feel free to set more while you're at it - these variables affect the
app's config - [see below](#pydantic-settings-config)):

```shell linenums="1" title=".env"
SESH_LOGGING=True
EXAMPLE_LOG_LEVEL=DEBUG
```

You can do this even when you're inside a git working directory, as gitignore
will ignore the .env file.

Once you've set these environment variables, restart the docker compose
services with

`docker compose -f docker-compose-example.yaml restart`

Now, you can tail the log file with

`docker logs -f sesh.example.api`

while you make requests of the Example API and observe the processing in the log. 


!!! tip

    We run our automated tests against the example app. We have included those
    tests in the docker image and they can be run at any time by invoking

    `docker exec -t sesh.example.api bash -c "/opt/sesh/bin/pytest -vv tests"`


## Pydantic Settings Config
This app makes use of a
[Pydantic Settings](https://docs.pydantic.dev/latest/concepts/pydantic_settings/)
object to store configuration. The config file in use for the example app is
shown below:

```python linenums="24"
from typing import Callable
from uuid import uuid4

from pydantic import RedisDsn
from pydantic_settings import BaseSettings, SettingsConfigDict


def new_key_id() -> str:
    """Generate a UUID4 value, but immediately cast it as a string.

    Returns: a string of a UUID4 value

    """
    return str(uuid4())


class Config(BaseSettings):
    model_config = SettingsConfigDict(env_prefix='example_')

    auth_cookie_salt: str
    auth_session_ttl: int

    backend: str
    cookie_domain: str

    deployment_stage: str

    session_cookie_http_only: bool
    session_cookie_salt: str
    session_cookie_same_site: str
    session_ttl: int

    project_name: str
    redis_dsn: RedisDsn
    secret_key: str

    version: str = "1.0"
    sid_generator: Callable = new_key_id

    def new_key_id(self) -> str:
        return self.sid_generator()


config = Config()  # type: ignore
```

We have a `.env` file similar to the one below that we automatically load into
the environment with [direnv](https://direnv.net/) whenever we enter the project
directory.  We also have these environment variables set on the GitLab CI/CD
pipelines for this project.  These environment variables automatically feed
into the Pydantic Settings config.

```shell linenums="1"
SESH_LOGGING=True
EXAMPLE_LOG_LEVEL=DEBUG

EXAMPLE_AUTH_COOKIE_SALT=auth_cookie_1970
EXAMPLE_AUTH_SESSION_TTL=7200

EXAMPLE_BACKEND=redis

EXAMPLE_COOKIE_DOMAIN=dev.trove.fm

EXAMPLE_REDIS_DSN=redis://redis:6379

EXAMPLE_SESSION_COOKIE_HTTP_ONLY=True
EXAMPLE_SESSION_COOKIE_SALT=session_cookie_1977
EXAMPLE_SESSION_COOKIE_SAME_SITE=strict
EXAMPLE_SESSION_TTL=604800
```

## Cookie Types

The number and types of cookies that can be processed by Sesh are limited mainly
by the ability to provide processing for the cookie.  If the cookie is simply
storing a key to be used with the backend to hydrate one of the app's models,
setting that up is pretty straightforward.  Let's start with those types of cookies.

Before we start out setting any cookies though, we need to extend the CookieType
enum provided by Sesh.  Though you'd think that extending a previously defined
enum was not possible, it is in this case because Sesh uses enum classes from
the [`aenum`](https://github.com/ethanfurman/aenum) library rather than from
the standard library.

!!! tip

    It's important to pay attention to the name of the module specified at the top
    of each code block shown below.

Here is the CookieType enum that gets imported at the top of `sesh.example.cookies`
```python linenums="29" title="sesh.example.cookies"
from sesh.models.cookie import CookieType
```

```python linenums="60" title="sesh.models.cookie"
class CookieType(UniqueEnum):
    """An enumeration of the types of cookies available and their associated data model
    """
    _init_ = 'model_class model'
    COOKIE = CookiePayload, 'CookiePayload'
```

Note how we extend the `CookieType` before we create our session factories:

```python linenums="38" hl_lines="15 16 17 18 19 20 21" title="sesh.example.cookies"

for name, value in (
    ('AUTH', (AuthData, 'AuthData')),
    ('SESSION', (SessionData, 'SessionData')),
):
    extend_enum(CookieType, name, value)
```
 Here we've just added two new cookie types to the CookieType enum.  We store
 two values for each name in the enum.  These values are passed together as
 a tuple to the `extend_enum` function.  The first value is the class for the
 data model to be hydrated with the data returned from the backend (in the case
 of the example app, the backend will be [Redis](https://redis.io/))  The
second value is a string representation of the name of the class used for the
data model.
 

## The CookieSession Factory
The core processing in Sesh occurs in a subclass of the Abstract Base Class
[`Session`](api_reference/session_base.md). The only subclass of Session that Sesh offers at the moment is the
[`CookieSession`](api_reference/session_cookie.md), which manages sessions using HTTP cookies.

We will create one CookieSession factory for each type of cookie that we want
to use in our app. If nothing else, you will want to create the 'auth' cookie
to ensure that your routes are only accessible by authenticated individuals.


## Creating the Session Factories

Below, we create each of two session factories to manage the two cookies
that we intend on setting when a user accesses this example API.

### Creating the Redis Backend
Notice that on line 32, we create the
[`redis_backend`](api_reference/backend_redis.md), which serves as the backend
for the two factories. This is a bigger deal than it looks or sounds like. The
docker compose file that we used also spun up a docker container for Redis and
this [`redis_backend`](api_reference/backend_redis.md) provides our interface to
that Redis db. All of the caching that's done is done through this Redis backend.
```python linenums="14" title="sesh.example.cookies" hl_lines="19 21 31"
from aenum import extend_enum

from sesh.backend.redis import RedisStore
from sesh.backend.base import SessionStore
from sesh.session.cookie import CookieSession
from sesh.models.cookie import CookieType

from .config import config
from .models.auth import AuthData
from .models.session import SessionData


for name, value in (
    ('AUTH', (AuthData, 'AuthData')),
    ('SESSION', (SessionData, 'SessionData')),
):
    extend_enum(CookieType, name, value)

redis_backend: SessionStore = RedisStore(config.redis_dsn)

auth_session = CookieSession(
    backend=redis_backend,
    cookie_type=CookieType.AUTH,
    created_by=1,
    domain=config.cookie_domain,
    max_age=config.auth_session_ttl,
    salt=config.auth_cookie_salt,
    secret_key=config.secret_key,
)

user_session = CookieSession(
    backend=redis_backend,
    cookie_type=CookieType.SESSION,
    created_by=1,
    domain=config.cookie_domain,
    max_age=config.session_ttl,
    salt=config.session_cookie_salt,
    secret_key=config.secret_key,
)
```

!!! warning

    Observant devs will notice that Sesh does in fact come with another
    backend in addition to `RedisStore`, that of `InMemoryStore`, a
    very basic python implementation of an in-memory database. Adventurers and
    the curious are encouraged to check it out if deired, but we DO NOT
    reccomend using the `InMemoryStore` as an actual backend for anything.

## Using the Session Factories
These session factories will get used directly in our app code and will be
passed as arguments to the Dependables in the API Routes.  We can see them
being used both ways below, in the example app's `login` route:

### Add the 'auth' cookie
Let's take a look at our `login` route.  The first thing we need to do is
determine if the user is authorized. We pass the `auth_session` session factory
to the `login_check` dependency on *__line 56__* to see if the user has an 'auth'
cookie that is still valid and if so, return AuthData for the user.

If no `auth_data` comes back from the Dependency, then the user is __not__
currently authenticated on the site.  We should remedy this by authenticating
the user against our persistent database/auth workflow. We demo this
idea here by implementing a `mock_database` that we import and use as our
`persistent_db`. Once the user is determined to be authenticated, we __create__
AuthData for them using the return from the authentication query and create
an entry for it in the Redis cache.

Finally, on lines 92 & 99, below, we pack the 'auth' cookie payload and attach
the 'auth' cookie to the response.

```python title="sesh.example.api" linenums="41" hl_lines="16 34 52 59"
router = APIRouter()


@router.post(
    "/login/",
    name="login",
    response_model=SessionData,
    response_model_exclude={"app_role", "groups", "key_id", "key_ttl"},
    response_model_exclude_none=True,
    status_code=status.HTTP_201_CREATED
)
async def login(
        body: dict,
        request: Request,
        response: Response,
        auth_data: dict = Depends(login_check(auth_session))
):
    authed_user = None
    if not auth_data:
        # In this example app, we assume that if they are in the database, they
        # are successfully authorized at login.  At a minimum, an actual login
        # function should include a check against a password hash.
        authed_user = persistent_db.get(body['email_address'])

        if authed_user:
            # If the user is found in the persistent_db, they are assumed to be authorized
            # Write the user's 'auth' data to the Redis cache.
            try:
                auth_data = AuthData(
                    key_id=config.new_key_id(),
                    key_ttl=config.auth_session_ttl,
                    **authed_user
                )
                await auth_session.backend.create_entry(auth_data)
            except ValidationError as e:
                raise HTTPException(status_code=422, detail=e.json(include_input=False).replace('"', ""))
            except SessionError:
                raise HTTPException(
                    status_code=503,
                    detail="There is a problem with the session validation service."
                )
        else:
            # User not found in database, so they must be a new user.
            # Ask them to sign-up.
            return RedirectResponse(url=f"http://{config.cookie_domain}/api/sign-up/")  # noqa
    else:
        # User's'auth' cookie from last visit is still valid.
        # 'Auth' data returned from cache.
        pass

    # Pack the cookie's payload and attach it to the response.
    payload = auth_session.pack_payload(
        CookiePayload(
            key_id=auth_data.key_id,
            key_ttl=auth_data.key_ttl,
            data_model=auth_session.cookie_type.model
        )
    )
    auth_session.attach_to_response(payload, response)
```

### Cookie Data Representation
Below we show representations of the cookie data at various stages in the
workflow.  The `key_id` is a UUID that is assigned to a particular user's
data that is stored with that key in a Redis database. Whenever we
want to access this state data, we can retrieve the key value from the cookie
and use it to look up the data in Redis.

```commandline
CookiePayload:
key_id='2b934a0f-f963-49c0-9f74-51d7fee1b50d' key_ttl=7200 data_model='AuthData' remainder=[]

Un-signed/un-encrypted 'packed' cookie value:
2b934a0f-f963-49c0-9f74-51d7fee1b50d.7200.AuthData

A signed/encrypted value:
IjJiOTM0YTBmLWY5NjMtNDljMC05Zjc0LTUxZDdmZWUxYjUwZC43MjAwLkF1dGhEYXRhIg.ZrexAg.kUgC01p0RsQcej6YEPdRvsEQfys
```

Below is a shot of the Redis Insight client, showing the data (auth_data)
in Redis associated with `key_id 2b934a0f-f963-49c0-9f74-51d7fee1b50d`

![redis_insight](assets/redis_auth_data.png)

### Add the 'session' cookie
As we did with the auth cookie, we do the same thing here and check to see
if the user presents a cookie for a currently valid 'session'. Here, we add
session data based on the return of the auth query from earlier, but we could
choose any query and model to use for this `SessionData`.

The `SessionData` model for the example app is defined in
`sesh.example.models.session` and is effectively a subclass of the `User` model
from `sesh.example.models.user`. The app designer can use anything that they
want to encode state in `SessionData`, though the model they use should be a
subclasss of `KeyBase` in `sesh.models.base` or of `CookiePayload` in `sesh.models.cookie`.

Here, the `session` cookie will store a `key_id` that is bound to a value in the
Redis store. This value in the Redis store can be retrieved using the key and
the data in the value can be used to hydrate a `SessionData` model. The
`SessionData` model is a subclass `User`, which is in turn a subclass of `KeyBase`.
Since the `SessionData` model class inherits from `KeyBase`, Sesh knows how to
handle it and the cookies associated with it.

```python title="sesh.example.api" linenums="101" hl_lines="2 12 22 29"
    # Get valid session data from the Redis cache, if it exists.
    session_data: SessionData = await user_session(request, response, login_route=True)

    if authed_user and not session_data:
        # Create new session data
        try:
            session_data: SessionData = SessionData(
                key_id=config.new_key_id(),
                key_ttl=config.session_ttl,
                **authed_user
            )
            await user_session.backend.create_entry(session_data)
        except ValidationError as e:
            raise HTTPException(status_code=422, detail=e.json(include_input=False).replace('"', ""))
        except SessionError:
            raise HTTPException(
                status_code=503,
                detail="There is a problem with the session validation service."
            )

    # Pack the cookie's payload and attach it to the response.
    user_session.pack_payload(
        CookiePayload(
            key_id=session_data.key_id,
            key_ttl=session_data.key_ttl,
            data_model=user_session.cookie_type.model
        )
    )
    user_session.attach_to_response(response)
```

## Using the Dependencies

### Get State Data
In our example app thus far, we've only covered the `login` route, which is a
bit of an extensive and somewhat complicated function, as we are checking/adding
two different cookies after the User successfully logs-in.

Often though, we may only be interested in determining if a user is authenticated
to access a particular route on an API. Although Sesh does __not__ yet provide
Role-Based Access Control (RBAC), or anything else that granular in this
release, we can identify if a user is authenticated with the API in general.
We can in other words determine between a guest visitor and a registered visitor
and control access accordingly, using Dependency Injection. Sesh provides [three
different dependencies](api_reference/authentication.md) for use in concert with
the session factories.

Take a look at the code for the `profile/` route below, which is far simpler
than the login code, but provides both authentication and cached access to
the data that we will return for that route.

```python title="sesh.example.api" linenums="159" hl_lines="9"
@router.get(
    "/profile/",
    response_model=SessionData,
    response_model_exclude={"app_role", "groups", "key_id", "key_ttl"},
    response_model_exclude_none=True,
    name="profile",
    status_code=status.HTTP_200_OK
)
async def get_profile(session_data: SessionData = Depends(get_state(user_session))):
    """
    User must be logged-in to see their profile
    """
    return session_data
```

This session data is the data that we have originally pulled from the
persistent_db and cached in Redis when the user logged-in. The key for
that redis entry is stored in the 'session' cookie.  The data in the
cache is also updated when the user posts changes to the `/profile/update/`
route.

When the session factory is passed to the `get_state` dependency and called,
the instance of the session factory is called. When this call is
made, the user is first authenticated, then if authenticated, a call for the
related state data for this session factory is made and that data is returned
to the caller.

### Authenticate for the Route

Sometimes you want something even simpler. You don't need any data back - you
just need the user authenticated for the route.  In that case, you can add the
`auth_session` to the `user_auth` dependable in the dependencies argument to
the path decorator.
```python title="sesh.example.api" linenums="198" hl_lines="7"
@router.post(
    "/content/new/",
    name="content:new",
    response_model=ContentData,
    response_model_exclude_none=True,
    status_code=status.HTTP_201_CREATED,
    dependencies=[Depends(user_auth(auth_session))]
)
async def post_new_content(content: ContentData):
    return content
```
This dependency will return a boolean value indicating whether or not the user
is authenticated for the route.
