
# Stateful Cookies

Not all use cases involve pulling some data out of cache based on a key.
Sometimes you may just want to store a piece of state directly in the cookie.
You can do this with Sesh, but it involves a little more work on your part,
as Sesh won't know how to process your cookie.

In lieu of providing a backend source to a session factory, you can instead
provide it with a custom packer, custom parser, and custom processor that Sesh
can use to handle this cookie.

We provide an example below, storing location state directly in the cookie,
based on ip-address lookup.  This is not actually in the `Example` app, but
if it were, this is where it would go.

```python linenums="24" title="example.models.geo" hl_lines="21 35 60"
import os
from typing import Optional, Union

from fastapi import Request, Response
from httpx import Client
from loguru import logger

from sesh.session.cookie import CookieSession
from sesh.models.cookie import CookiePayload

from ..config import config


class GeoCookiePayload(CookiePayload):
    ip_addr: str
    lat: Optional[float]
    long: Optional[float]
    location_name: Optional[str]


def geo_cookie_packer(factory_instance: CookieSession, payload: GeoCookiePayload):
    payload_string = '.'.join([
        payload.key_id,
        str(payload.key_ttl),
        payload.data_model,
        str(payload.ip_addr).replace('.', '_'),
        str(payload.lat).replace('.', '_'),
        str(payload.long).replace('.', '_'),
        payload.location_name.replace(', ', '-').replace(' ', '_')
    ])
    
    return payload_string


def geo_cookie_parser(factory_instance: CookieSession, signed_payload: Union[str, bytes]):
    decrypted_payload: str = factory_instance.get_cookie_payload(signed_payload)
    logger.error(f"DECRYPTED PAYLOAD: {decrypted_payload}")
    parts: list = decrypted_payload.split('.')
    payload: GeoCookiePayload = GeoCookiePayload(
        key_id=parts[0],
        key_ttl=int(parts[1]),
        data_model=parts[2],
        ip_addr=parts[3].replace('_', '.'),
        lat=parts[4].replace('_', '.'),
        long=parts[5].replace('_', '.'),
        location_name=parts[6].replace('_', ' ').replace('-', ', ')
    )

    return payload


def lookup_geo(host: str):
    client = Client(follow_redirects=True, headers={"Content-Type": "application/json"})
    url = f'https://api.ipgeolocation.io/ipgeo?apiKey={config.ipgeo_api_key}&ip={host}'  # noqa
    geo_data = client.get(url)

    return geo_data.json()


def geo_cookie_processor(
        factory_instance: CookieSession,
        request: Request,
        response: Response,
        payload: GeoCookiePayload = None
):
    init_payload: Optional[GeoCookiePayload] = None
    if not payload:
        host = request.headers.get('X-Sesh-Geo-IP')
        if not host:
            host = request.client.host
        logger.debug(f"ENV COOKIE DOMAIN: {os.environ.get('EXAMPLE_COOKIE_DOMAIN')}")
        logger.debug(f"CONFIG COOKIE DOMAIN: {config.cookie_domain}")
        logger.info(f"INITIAL HOST: {host}")
        gd = lookup_geo(host)
        init_payload = GeoCookiePayload(
            key_id='0',
            key_ttl=0,
            data_model='GeoCookiePayload',
            ip_addr=host,
            lat=gd['latitude'],
            long=gd['longitude'],
            location_name=f"{gd['city']}, {gd['state_prov']}, {gd['country_name']}"
        )

    if init_payload:
        geo_cookie_packer(factory_instance, init_payload)
        payload = init_payload
    else:
        host = request.headers.get('X-Sesh-Geo-IP')
        if not host:
            host = request.client.host
        logger.info(f"HOST: {host}")
        if host != payload.ip_addr:
            new_gd = lookup_geo(host)
            new_payload = GeoCookiePayload(
                key_id='0',
                key_ttl=0,
                data_model='GeoCookiePayload',
                ip_addr=host,
                lat=new_gd['latitude'],
                long=new_gd['longitude'],
                location_name=f"{new_gd['city']}, {new_gd['state_prov']}, {new_gd['country_name']}"
            )
            geo_cookie_packer(factory_instance, new_payload)
            payload = new_payload
        else:
            geo_cookie_packer(factory_instance, payload)
    factory_instance.attach_to_response(response)
```

You will need to provide the highlighted `geo_cookie_packer`, `geo_cookie_parser`,
and `geo_cookie_processor` when creating the geo session factory:

```python linenums="74" title="example.cookies"
geo_session = CookieSession(
    cookie_packer=geo_cookie_packer,
    cookie_parser=geo_cookie_parser,
    cookie_processor=geo_cookie_processor,
    cookie_type=CookieType.GEO,
    created_by=1,
    domain=config.cookie_domain,
    httponly=False,
    max_age=config.geo_cookie_max_age,
    path='/api/login/',
    signer=None,
)
```

We might then access and set a geo cookie like this:

```python linenums="131" title="example.api"
    # Set a Geo Cookie with location info
    signed_geo_payload = request.cookies.get('geo')
    payload = geo_session.parse_cookie(signed_geo_payload)
    geo_session.process_cookie(request, response, payload)

```
