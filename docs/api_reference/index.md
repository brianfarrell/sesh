
# API Reference

## Backends
- [RedisStore](backend_redis.md)
- [SessionStore (Abstract Base Class)](backend_base.md)


## Dependencies
- [Authentication](authentication.md)


## Models
- [Cookie](model_cookie.md)
- [KeyBase](model_key.md)


## Session Factories 
- [CookieSession](session_cookie.md)
- [Session (Abstract Base Class)](session_base.md)
