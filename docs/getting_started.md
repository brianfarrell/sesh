
# Getting Started

## Installation

Install with pip:
```console
pip install sesh
```

The next steps to get Sesh working with your FastAPI app include:

- [Extending the CookieType](example_app.md#cookie-types) enum to reference
  your cookie types
- [Creating the backend](example_app.md#creating-the-redis-backend) that you
  will use to cache your data
- [Creating the session factories](example_app.md#creating-the-session-factories)
  that you will use to manage your sessions
- [Using the session factories](example_app.md#using-the-session-factories) to
  Create/Read/Update/Delete entries in the backend cache
- [Using the session factories](example_app.md#using-the-session-factories) to
  pack your cookie data and set your cookies
- [Using dependencies](example_app.md#get-state-data) to retrieve state data from cache
- [Using dependencies](example_app.md#authenticate-for-the-route) to authenticate the user for the route
