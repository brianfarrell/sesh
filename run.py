
"""
sesh is a session management library for FastAPI

Copyright (C) 2024  Brian Farrell

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Contact: brian.farrell@me.com
"""


import logging
import os
import sys

from loguru import logger
from uvicorn import Config, Server
from uvicorn.supervisors import ChangeReload, Multiprocess


LOG_LEVEL = logging.getLevelName(os.environ.get("EXAMPLE_LOG_LEVEL", "WARNING"))
JSON_LOGS = True if os.environ.get("EXAMPLE_JSON_LOGS", "False") == "True" else False


class InterceptHandler(logging.Handler):
    def emit(self, record):
        # Get corresponding Loguru level if it exists
        try:
            level = logger.level(record.levelname).name
        except ValueError:
            level = record.levelno

        # Find caller from where originated the logged message
        frame, depth = logging.currentframe(), 2
        while frame.f_code.co_filename == logging.__file__:
            frame = frame.f_back
            depth += 1

        logger.opt(depth=depth, exception=record.exc_info).log(level, record.getMessage())


def setup_logging():
    # intercept everything at the root logger
    logging.root.handlers = [InterceptHandler()]
    logging.root.setLevel(LOG_LEVEL)

    # remove every other logger's handlers
    # and propagate to root logger
    for name in logging.root.manager.loggerDict.keys():
        logging.getLogger(name).handlers = []
        logging.getLogger(name).propagate = True

    # configure loguru
    logger.configure(handlers=[{"sink": sys.stdout, "serialize": JSON_LOGS, "colorize": True}])


if __name__ == '__main__':
    app = "sesh.example.main:get_app"

    STARTUP_FAILURE = 3

    config = Config(
        app,
        host="0.0.0.0",
        port=8001,
        log_level=LOG_LEVEL,
        reload=True,
        reload_dirs=["sesh/"],
        workers=1,
        factory=True
    )

    # monkey patch configure_logging in Config so that we continue to get nice logging on reloads
    config.configure_logging = setup_logging

    server = Server(config)

    setup_logging()  # Still necessary here, in spite of the monkey patch, for initial launch.

    # took the code below from uvicorn.main, so that we can properly support reloading
    if (config.reload or config.workers > 1) and not isinstance(app, str):
        logger.warning(
            "You must pass the application as an import string to enable 'reload' or "
            "'workers'."
        )
        sys.exit(1)

    if config.should_reload:
        sock = config.bind_socket()
        ChangeReload(config, target=server.run, sockets=[sock]).run()
    elif config.workers > 1:
        sock = config.bind_socket()
        Multiprocess(config, target=server.run, sockets=[sock]).run()
    else:
        server.run()

    if config.uds:
        os.remove(config.uds)  # pragma: py-win32

    if not server.started and not config.should_reload and config.workers == 1:
        sys.exit(STARTUP_FAILURE)
